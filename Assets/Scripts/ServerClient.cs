using UnityEngine;
using Proyecto26;
using Models;

public class ServerClient : LazySingleton<ServerClient>
{
    private readonly string apiPath = "http://localhost:3000";
    private RequestHelper currentRequest;
    
    public void CreatePlayer(string playerName, System.Action<Player> callback)
    {
        currentRequest = new RequestHelper
        {
            Uri = apiPath + "/users",
            Body = new Player { name = playerName },
            EnableDebug = true
        };
        RestClient.Post<Player>(currentRequest)
            .Then(callback)
            .Catch(err => {
                Debug.Log(err.Message);
            });
    }

    public void StartGameSeek()
    {
        var user_id = PlayerManager.Instance.playerData.id;
        Debug.Log($"Start server request for game seek. User id: {user_id}");
        currentRequest = new RequestHelper
        {
            Uri = apiPath + $"/users/{user_id}/games/seek",
            EnableDebug = true
        };
        RestClient.Get(currentRequest)
            .Then(res => {
                Debug.Log("Complete server request for game seek");
            })
            .Catch(err => {
                Debug.Log("Error request" + err.Message);
            });
    }

    public void AbortRequest()
    {
        if (currentRequest != null)
        {
            currentRequest.Abort();
            currentRequest = null;
        }
    }
}
