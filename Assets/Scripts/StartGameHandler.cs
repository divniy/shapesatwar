using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartGameHandler : MonoBehaviour
{
    private Button button;
    private TMP_Text buttonText;
    private bool isGameStarted;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        buttonText = GetComponentInChildren<TMP_Text>();
        PrepareStartButton();
        button.onClick.AddListener(OnClick);
    }

    void PrepareStartButton()
    {
        buttonText.text = "Играть";
        isGameStarted = false;
    }

    void PrepareCancelButton()
    {
        buttonText.text = "Отмена";
        isGameStarted = true;
    }

    void OnClick()
    {
        if (isGameStarted)
        {
            CancelGame();
            PrepareStartButton();
        }
        else
        {
            StartGame();
            PrepareCancelButton();
        }
    }

    void StartGame()
    {
        Debug.Log("Start game");
        PlayerManager.Instance.StartGameSeek();
    }

    void CancelGame()
    {
        Debug.Log("Cancel game");
    }
}
