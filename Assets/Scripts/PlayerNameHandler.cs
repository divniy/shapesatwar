using UnityEngine;
using TMPro;

public class PlayerNameHandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TMP_Text nameText = GetComponent<TMP_Text>();
        if(nameText != null)
        {
            nameText.text = PlayerManager.Instance.playerData.name;
        }
    }
}
