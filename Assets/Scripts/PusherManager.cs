using System;
using System.Threading.Tasks;
using PusherClient;
using UnityEngine;

public class PusherManager : LazySingleton<PusherManager>
{
    private Pusher pusher;
    private Channel channel;

    // Start is called before the first frame update
    //async Task Start()
    //{
    //    await InitialisePusher();
    //}

    public async Task InitialisePusher()
    {

        if (pusher == null)
        {
            pusher = new Pusher("ced1f399f1a9893256c7", new PusherOptions()
            {
                Cluster = "eu",
                Encrypted = true
            });

            pusher.Error += OnPusherOnError;
            pusher.ConnectionStateChanged += PusherOnConnectionStateChanged;
            pusher.Connected += PusherOnConnected;
            channel = await pusher.SubscribeAsync($"user-{PlayerManager.Instance.playerData.id}");
            channel.Subscribed += OnChannelOnSubscribed;
            await pusher.ConnectAsync();
        }
    }

    private void PusherOnConnected(object sender)
    {
        Debug.Log("Connected");
        channel.Bind("game-start", (dynamic data) =>
        {
            Debug.Log($"game-start received with: {data}");
        });
    }

    private void PusherOnConnectionStateChanged(object sender, ConnectionState state)
    {
        Debug.Log("Connection state changed");
    }

    private void OnPusherOnError(object s, PusherException e)
    {
        Debug.Log("Errored");
    }

    private void OnChannelOnSubscribed(object s)
    {
        Debug.Log("Subscribed");
        ServerClient.Instance.StartGameSeek();
    }

    async Task OnApplicationQuit()
    {
        if (pusher != null)
        {
            await pusher.DisconnectAsync();
        }
    }
}
