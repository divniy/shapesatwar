﻿using System;

namespace Models
{
    [Serializable]
    public class Player
    {
        public int id;
        public string name;
        public int score;

        public override string ToString()
        {
            return UnityEngine.JsonUtility.ToJson(this, true);
        }
    }
}