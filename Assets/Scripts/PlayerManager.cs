using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Models;

public class PlayerManager : LazySingleton<PlayerManager>
{
    public class PlayerEvent : UnityEvent<Player> { }
    public Player playerData = new Player();
    public PlayerEvent onPlayerCreated = new PlayerEvent();

    // Start is called before the first frame update
    void Start()
    {
        onPlayerCreated.AddListener(SetPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreatePlayerWithName(string playerName)
    {
        ServerClient.Instance.CreatePlayer(playerName, onPlayerCreated.Invoke);
    }

    void SetPlayer(Player player)
    {
        playerData.id = player.id;
        playerData.name = player.name;
        playerData.score = player.score;
    }

    public void StartGameSeek()
    {
        ServerClient.Instance.StartGameSeek();
        //PusherManager.Instance.InitialisePusher().ContinueWith(t =>
        //{
            //Debug.Log($"Initialized: {t}"); 
        //});
        
    }
}
