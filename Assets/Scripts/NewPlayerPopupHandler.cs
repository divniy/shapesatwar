using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Models;

public class NewPlayerPopupHandler : MonoBehaviour
{
    public TMP_InputField playerNameField;
    public Button savePlayerButton;
    public GameObject lobby;

    // Start is called before the first frame update
    void Start()
    {
        savePlayerButton.onClick.AddListener(SaveButtonPressed);
        PlayerManager.Instance.onPlayerCreated.AddListener(OnPlayerCreated);
    }

    public void SaveButtonPressed()
    {
        Debug.Log("Save player button pressed. " + $"Player name: {playerNameField.text}");
        PlayerManager.Instance.CreatePlayerWithName(playerNameField.text);
        //if (usernameField == null) { return; }
        //ServerRestAPI.Instance.CreatePlayerRequest(usernameField.text)
        //    .Then(() => { hidePopup(); });
    }

    void OnPlayerCreated(Player player)
    {
        hidePopup();
        showLobby();
    }

    void hidePopup()
    {
        gameObject.SetActive(false);
    }

    void showLobby()
    {
        lobby.SetActive(true);
    }    
}
